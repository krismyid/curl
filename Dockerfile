FROM alpine:latest

RUN apk add --no-cache curl bash jq openssl
RUN apk add --no-cache --update coreutils

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]